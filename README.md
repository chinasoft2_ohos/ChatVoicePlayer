# ChatVoicePlayer

#### 项目介绍
- 项目名称：简单音乐播放器
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现了简单音乐播放器功能，实现播放、暂停功能。
- 未实现功能：因为ohos目前暂不支持系统分享原因，通过intent分享功能没有实现
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本： Release Tags 1.0.9

#### 效果演示
<img src="img/aaa.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ChatVoicePlayer:1.0.0')
    ......  
 }
```

#### 使用说明

组件主要由PlayerVisualizerSeekbar实现自定义进度条样式和VoicePlayerView实现播放器，播放、暂停功能组成。

1、您可以添加到您的xml布局：
 ```
 <me.jagar.chatvoiceplayerlibrary.VoicePlayerView
         ohos:id="$+id:voicePlayerView"
         ohos:height="match_content"
         ohos:width="match_content"
         app:enableVirtualizer="true"
         app:playPauseBackgroundColor="#ff7700"
         app:playPauseCornerRadius="100"
         app:playProgressbarColor="#ff7700"
         app:progressTimeColor="#000"
         app:seekBarProgressColor="#000"
         app:seekBarThumbColor="#FFC107"
         app:shareBackgroundColor="#ff7700"
         app:shareCornerRadius="100"
         app:shareText="SHARE ME"
         app:showShareButton="true"
         app:showTiming="true"
         app:timingBackgroundColor="#6AFF7700"
         app:viewBackground="#C6C4CF"
         app:viewCornerRadius="100"
         app:visualizationNotPlayedColor="#727171"
         app:visualizationPlayedColor="#ff7700"/>
```
2、在MainAbilitySlice中找到控件设置播放音频文件。
```
  VoicePlayerView voicePlayerView = (VoicePlayerView) findComponentById(ResourceTable.Id_voicePlayerView);
      try {
          RawFileDescriptor filDescriptor = getResourceManager()
                    .getRawFileEntry("resources/rawfile/qnzl.mp3").openRawFileDescriptor();
          Source source = new Source(filDescriptor.getFileDescriptor(),
                    filDescriptor.getStartPosition(), filDescriptor.getFileSize());
          File file = getFile(this);
          voicePlayerView.setAudio(source, FileUtils.fileToBytes(file));
      } catch (Exception e) {
          System.out.println(e);
      }
```
支持功能：
```
onStop() //This will avoid many errors if you called it on your activity onStrop() <br>
onPause() //If you don't want to play the voice out of your activity/app! <br>
setViewBackgroundShape(int color, float radius)` //color should be R.color.YOUR_COLOR <br>
setShareBackgroundShape(int color, float radius)` //color should be R.color.YOUR_COLOR <br>
setPlayPaueseBackgroundShape(int color, float radius)` //color should be R.color.YOUR_COLOR <br>
setSeekBarStyle(int progressColor, int thumbColor)` //both colors should be R.color.YOUR_COLOR <br>
setShareButtonVisibility(boolean visibility)
setTimingVisibility(boolean visibility)
refreshPlayer(Source source)
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
MIT License

Copyright (c) 2019 Jagar Yousef

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```