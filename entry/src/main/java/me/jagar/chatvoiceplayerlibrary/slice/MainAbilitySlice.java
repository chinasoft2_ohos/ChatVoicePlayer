/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.jagar.chatvoiceplayerlibrary.slice;

import me.jagar.chatvoiceplayerlibrary.FileUtils;
import me.jagar.chatvoiceplayerlibrary.ResourceTable;
import me.jagar.chatvoiceplayerlibrary.VoicePlayerView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.global.resource.RawFileDescriptor;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.media.common.Source;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 测试
 *
 * @author ljx
 * @since 2021-05-08
 */
public class MainAbilitySlice extends AbilitySlice {
    private static final int CAPACITY = 1024;
    private static final int CONSTANT = -1;
    private VoicePlayerView voicePlayerView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        voicePlayerView = (VoicePlayerView) findComponentById(ResourceTable.Id_voicePlayerView);
        try {
            RawFileDescriptor filDescriptor = getResourceManager()
                    .getRawFileEntry("resources/rawfile/qnzl.mp3").openRawFileDescriptor();
            Source source = new Source(filDescriptor.getFileDescriptor(),
                    filDescriptor.getStartPosition(), filDescriptor.getFileSize());
            File file = getFile(this);
            voicePlayerView.setAudio(source, FileUtils.fileToBytes(file));
        } catch (Exception e) {
            new ToastDialog(this).setText(e.getMessage()).show();
        }
    }

    /**
     * getFile
     *
     * @param context context
     * @return File
     */
    private File getFile(Context context) {
        ResourceManager resManager = getContext().getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry("resources/rawfile/qnzl.mp3");
        Resource resource = null;
        File file = null;
        OutputStream outputStream = null;
        try {
            resource = rawFileEntry.openRawFile();
            file = new File(context.getExternalFilesDir(Environment.DIRECTORY_MUSIC), "qnzl.mp3");
            outputStream = new FileOutputStream(file);

            int index;
            byte[] bytes = new byte[CAPACITY];
            while ((index = resource.read(bytes)) != CONSTANT) {
                outputStream.write(bytes, 0, index);
                outputStream.flush();
            }
        } catch (IOException e) {
            new ToastDialog(this).setText(e.getMessage()).show();
        } finally {
            try {
                resource.close();
                outputStream.close();
            } catch (Exception e) {
                new ToastDialog(this).setText(e.getMessage()).show();
            }
        }
        return file;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        voicePlayerView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        voicePlayerView.onStop();
    }
}