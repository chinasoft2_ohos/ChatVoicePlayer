/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.jagar.chatvoiceplayerlibrary;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * 测试
 *
 * @author ljx
 * @since 2021-05-08
 */
public class ExampleOhosTest {

    private static VoicePlayerView mVoicePlayerView;

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("me.jagar.chatvoiceplayerlibrary", actualBundleName);
    }


    @BeforeClass
    public static void set() {
        Ability ability = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        mVoicePlayerView = new VoicePlayerView(ability.getContext(), null);
        mVoicePlayerView.setPlayPaueseBackgroundColor(Color.RED);
        mVoicePlayerView.setShareBackgroundColor(Color.RED);
        mVoicePlayerView.setViewBackgroundColor(Color.RED);
        mVoicePlayerView.setSeekBarProgressColor(Color.RED);
        mVoicePlayerView.setSeekBarThumbColor(Color.RED);
    }

    @Test
    public void getPlayPaueseBackgroundColor() {
        assertTrue(Color.RED.getValue() == mVoicePlayerView.getPlayPaueseBackgroundColor().getValue());
    }

    @Test
    public void getShareBackgroundColor() {
        assertTrue(Color.RED.getValue() == mVoicePlayerView.getShareBackgroundColor().getValue());
    }

    @Test
    public void getViewBackgroundColor() {
        assertTrue(Color.RED.getValue() == mVoicePlayerView.getViewBackgroundColor().getValue());
    }

    @Test
    public void getSeekBarProgressColor() {
        assertTrue(Color.RED.getValue() == mVoicePlayerView.getSeekBarProgressColor().getValue());
    }

    @Test
    public void getSeekBarThumbColor() {
        assertTrue(Color.RED.getValue() == mVoicePlayerView.getSeekBarThumbColor().getValue());
    }


}