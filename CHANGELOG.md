## 1.0.0
ohos 第3个版本
* 生产版本

## 0.0.2-SNAPSHOT
ohos 第2个版本
* 更新sdk6

## 0.0.1-SNAPSHOT
ohos 第1个版本
 * 实现了原库的大部分 api
 * 因为ohos目前暂不支持系统分享原因，通过intent分享功能没有实现